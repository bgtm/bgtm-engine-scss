var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass')(require('sass'));
var rename = require('gulp-rename');
var deepExtend = require('deep-extend');
var autoprefixer = require('gulp-autoprefixer');

/**
 * This the default engine for building SASS into a consolidated build
 * for many projects.
 *
 * This ensures consistent build practices across many developers within a team.
 *
 * @param taskManager
 * @param args
 * @returns {*}
 */
module.exports = function (taskManager, args, done) {
    var defaults = {
        src: '',
        dest: '',
        autoprefixer: {
            cascade: false
        },
        sass: {
            style: 'expanded'
        }
    };

    var engineOptions = deepExtend({}, defaults, args);

    if (taskManager.isEnvironment(taskManager.environment.PRODUCTION)) {
        engineOptions.sass.style = 'compressed';
    }

    return this.src(engineOptions.src)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass(engineOptions.sass).on('error', sass.logError))
        .pipe(autoprefixer(engineOptions.autoprefixer))
        .pipe(rename({extname: '.min.css'}))
        .pipe(sourcemaps.write('./'))
        .pipe(this.dest(engineOptions.dest))
        .pipe(done());
};