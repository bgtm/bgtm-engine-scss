# BGTM - Engine for Sass Compilation

This is an engine which can be used in conjunction with
[BGTM](https://www.npmjs.com/package/bgtm) for
the purposes of compiling sass files in a consistent and repeatable
manner across every project in an agency.

# How to use

Read the how to section for [BGTM](https://www.npmjs.com/package/bgtm).

Run:

    npm install --save-dev bgtm-engine-scss

In your gulpfile add the following dependency:

    var scss = require('bgtm-engine-scss');

Then add the following task.

    tm.add('scss', {
        runOnBuild: true,
        watch: true,
        watchSource: [
            'src/css/**/*.scss'
        ],
        liveReload: true,
        engine: scss,
        engineOptions: {
            'src': 'src/css/main.scss',
            'dest': 'dest/css/'
        }
    });

# What it does

* Compiles the scss file(s) specified in the `src` property using [gulp-sass](https://www.npmjs.com/package/gulp-sass)
* Runs [gulp-autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer) to automatically prefix any vendor specific prefixes to properties that are not yet fully released (e.g. `-ms-`, `-webkit-`, `-moz-`).
* Changes the extension of the output file to `.min.css`
* Generates and outputs a sourcemap file.
* Outputs them to the destination specified in the `dest` property.

Note: The compilation process is done using the outputStyle of
`expanded` unless BGTM is run in production mode.

You can do this with the following argument `gulp [optional task] --production`

# Engine Options

## Defaults

    {
        src: [],
        dest: '',
        autoprefixer: {
            browsers: [
                "iOS 7",
                "iOS 8",
                "last 2 versions"
            ],
            cascade: false
        }
    }

| Option       | Mandatory | Description |
|--------------|-----------|-------------|
| src          | yes       | The source path to look for changes, this can be an array of strings or a string. |
| dest         | yes       | The destination file for the compiled output to be placed into. |
| autoprefixer | no        | Can be configured using any options available in the [gulp-autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer) package |

# License

Copyright 2017 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.